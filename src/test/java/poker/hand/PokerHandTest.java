package poker.hand;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import poker.card.Value;

import static org.junit.jupiter.api.Assertions.*;

public class PokerHandTest {
  @DisplayName("isRoyalFlush()")
  @Test
  void isRoyalFlushTest() {
    assertSame(HandCreator.createPokerHand("JC QC KC AC TC").getRank(), Rank.RoyalFlush);
    assertSame(HandCreator.createPokerHand("JC QC KC AC TC").getRankValue(), Value.Ace);
    assertNotSame(HandCreator.createPokerHand("JC QC KC 9C TS").getRank(), Rank.RoyalFlush);
    assertNotSame(HandCreator.createPokerHand("JC QC KC 9C TC").getRank(), Rank.RoyalFlush);
  }

  @DisplayName("isStraightFlush()")
  @Test
  void isStraightFlushTest() {
    assertSame(HandCreator.createPokerHand("JC QC KC 9C TC").getRank(), Rank.StraightFlush);
    assertSame(HandCreator.createPokerHand("JC QC KC 9C TC").getRankValue(), Value.King);
    assertNotSame(HandCreator.createPokerHand("2C 3C 4C 5C 6H").getRank(), Rank.StraightFlush);
    assertNotSame(HandCreator.createPokerHand("JC QC KC AC TC").getRank(), Rank.StraightFlush);
  }

  @DisplayName("isFourOfAKind()")
  @Test
  void isFourOfAKindTest() {
    assertSame(HandCreator.createPokerHand("2C 2H 2S 2D 6S").getRank(), Rank.FourOfAKind);
    assertSame(HandCreator.createPokerHand("2C 2H 2S 2D 6S").getRankValue(), Value.Two);
    assertNotSame(HandCreator.createPokerHand("JC QC KC AC TC").getRank(), Rank.FourOfAKind);
  }

  @DisplayName("isFullHouse()")
  @Test
  void isFullHouseTest() {
    assertSame(HandCreator.createPokerHand("JC TS JS TH JH").getRank(), Rank.FullHouse);
    assertSame(HandCreator.createPokerHand("JC TS JS TH JH").getRankValue(),Value.Jack);
    assertNotSame(HandCreator.createPokerHand("JC QC KC AC TC").getRank(), Rank.FullHouse);
  }

  @DisplayName("isFlush()")
  @Test
  void isFlushTest() {
    assertSame(HandCreator.createPokerHand("8C 9C TC JC AC").getRank(), Rank.Flush);
    assertSame(HandCreator.createPokerHand("8C 9C TC JC AC").getRankValue(), Value.Ace);
    assertNotSame(HandCreator.createPokerHand("8C TC KC 9C 4S").getRank(), Rank.Flush);
  }

  @DisplayName("isStraight()")
  @Test
  void isStraightTest() {
    assertSame(HandCreator.createPokerHand("2C 3C 4C 5C 6H").getRank(), Rank.Straight);
    assertSame(HandCreator.createPokerHand("2C 3C 4C 5C 6H").getRankValue(), Value.Six);
    assertNotSame(HandCreator.createPokerHand("JC QC KC 9C TC").getRank(), Rank.Straight);
  }

  @DisplayName("isThreeOfAKind()")
  @Test
  void isThreeOfAKindTest() {
    assertSame(HandCreator.createPokerHand("2C 2H 2S AD TS").getRank(), Rank.ThreeOfAKind);
    assertSame(HandCreator.createPokerHand("2C 2H 2S AD TS").getRankValue(), Value.Two);
    assertNotSame(HandCreator.createPokerHand("2C 2H 2S AD AS").getRank(), Rank.ThreeOfAKind);
    assertNotSame(HandCreator.createPokerHand("JC QC KC AC TC").getRank(), Rank.ThreeOfAKind);
  }

  @DisplayName("isTwoPairs()")
  @Test
  void isTwoPairsTest() {
    assertSame(HandCreator.createPokerHand("2C 2H QS QH AS").getRank(), Rank.TwoPairs);
    assertSame(HandCreator.createPokerHand("2C 2H QS QH AS").getRankValue(), Value.Queen);
    assertSame(HandCreator.createPokerHand("2C 2H QS QH AS").getTwoPairsFirstPairValue(), Value.Two);
    assertNotSame(HandCreator.createPokerHand("JC QC KC AC TC").getRank(), Rank.TwoPairs);
  }


  @DisplayName("isOnePair()")
  @Test
  void isOnePairTest() {
    assertSame(HandCreator.createPokerHand("2C 2H KS AD QS").getRank(), Rank.OnePair);
    assertSame(HandCreator.createPokerHand("2C 2H KS AD QS").getRankValue(), Value.Two);
    assertNotSame(HandCreator.createPokerHand("JC QC KC AC TC").getRank(), Rank.OnePair);
  }

  @DisplayName("isHighCard()")
  @Test
  void isHighCardTest() {
    assertSame(HandCreator.createPokerHand("JC QC KC AC 9H").getRank(), Rank.HighCard);
    assertSame(HandCreator.createPokerHand("JC QC KC AC 9H").getRankValue(), Value.Ace);
    assertNotSame(HandCreator.createPokerHand("JC QC KC AC TH").getRank(), Rank.HighCard);
    assertNotSame(HandCreator.createPokerHand("2C 2H KS AD 6S").getRank(), Rank.HighCard);
  }

}
