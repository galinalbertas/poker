package poker.hand;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import poker.card.Suite;
import poker.card.Value;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class HandTest {

  Hand hand = HandCreator.createHand("QC AD TC JS KH");

  @DisplayName("handTest()")
  @Test
  void handTest() {
    String handString = "[QC, AD, TC, JS, KH]";
    assertEquals(handString, hand.toString());
  }

  @DisplayName("handSort()")
  @Test
  void sortByValue() {
    String handString = "[TC, JS, QC, KH, AD]";
    hand.sortByValue();
    assertEquals(handString, hand.toString());
  }

  @DisplayName("getHandSortedByValue()")
  @Test
  void getHandSortedByValue() {
    String handString = "[TC, JS, QC, KH, AD]";
    assertEquals(handString, hand.getHandSortedByValue().toString());
  }

  @DisplayName("getCardsSortedByValue()")
  @Test
  void getCardsSortedByValue() {
    String handString = "[TC, JS, QC, KH, AD]";
    assertEquals(handString, hand.getCardsSortedByValue().toString());
  }

  @DisplayName("getCardValueList()")
  @Test
  void getCardValueList() {
    String handString = "[Queen, Ace, Ten, Jack, King]";
    assertEquals(handString, hand.getCardValueList().toString());
  }

  @DisplayName("getCardSuiteList()")
  @Test
  void getCardSuiteList() {
    String handString = "[Clubs, Diamonds, Clubs, Spades, Hearts]";
    assertEquals(handString, hand.getCardSuiteList().toString());
  }

  @DisplayName("getSuiteSet()")
  @Test
  void getSuiteSet() {
    Set<Suite> suiteSet=new HashSet<>(Arrays.asList(Suite.Diamonds, Suite.Clubs, Suite.Hearts, Suite.Spades));
    assertEquals(suiteSet, hand.getSuiteSet());
  }

  @DisplayName("getHighestValue()")
  @Test
  void getHighestValue() {
    assertEquals(Value.Ace, hand.getHighestValue());
  }

  @DisplayName("isOneSuite()")
  @Test
  void isOneSuite() {
    assertFalse(hand.isOneSuite());
    Hand handPositive = HandCreator.createHand("QC AC TC JC KC");
    assertTrue(handPositive.isOneSuite());
  }

  @DisplayName("getSecondHighestValue()")
  @Test
  void getSecondHighestValue() {
    assertEquals(Value.King, hand.getSecondHighestValue());
  }

  @DisplayName("getThirdHighestValue()")
  @Test
  void getThirdHighestValue() {
    assertEquals(Value.Queen, hand.getThirdHighestValue());
  }
}
