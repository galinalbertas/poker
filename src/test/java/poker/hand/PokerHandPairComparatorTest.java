package poker.hand;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class PokerHandPairComparatorTest {

  public static List<String> functionalTest() {
    List<String> winnerList = new ArrayList<>();
    PokerHandPairReader
      .readPairs("test.txt")
      .forEach(hp -> {
        System.out.println(hp[0].toString() + " " + hp[1].toString() + " " + (new PokerHandPairComparator(hp[0], hp[1]).compareWhy()));
        System.out.printf("  %-12s%-8s%-12s%-8s\n", hp[0].getRank(), hp[0].getRankValue(), hp[1].getRank(), hp[1].getRankValue());
        winnerList.add(new PokerHandPairComparator(hp[0], hp[1]).compare());
      });
    return winnerList;
  }

  @DisplayName("compareTest()")
  @Test
  void compareTest() {
    List<String> expected = Arrays.asList(
      "Player 2,Player 1,Player 2,Player 1,Player 1,Player 2,Player 2,Player 1,Player 2,Player 2,Player 1,Player 2,Player 2,Tie,Player 2,Tie"
        .split(","));

    List<String> winnerList = functionalTest();
    assertEquals(winnerList, expected);
  }

  @DisplayName("countTest()")
  @Test
  void countTest() {
    assertEquals(PokerHandPairReader
                   .readPairs("test.txt")
                   .stream()
                   .map(hp -> new PokerHandPairComparator(hp[0], hp[1]).compare())
                   .filter(PokerHandPairComparator.PLAYER_1::equals)
                   .count(), 5L);
  }
}
