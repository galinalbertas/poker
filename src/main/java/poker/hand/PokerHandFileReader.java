package poker.hand;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

public class PokerHandFileReader {
  public static List<String> read(String fileName) {
    List<String> pokerHands = new ArrayList<>();
    try (Stream<String> stream = Files.lines(Paths.get(fileName))) {
      stream.forEach(pokerHands::add);
    }
    catch (IOException e) {
      e.printStackTrace();
    }
    return pokerHands;
  }
}
