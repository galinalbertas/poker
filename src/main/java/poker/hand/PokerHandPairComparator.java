package poker.hand;

import poker.card.Value;

public class PokerHandPairComparator {

  private static final String TIE = "Tie";
  public static final String PLAYER_1 = "Player 1";
  public static final String PLAYER_2 = "Player 2";

  PokerHand firstHand;
  PokerHand secondHand;
  Value firstHandRankValue;
  Value secondHandRankValue;

  public PokerHandPairComparator(PokerHand firstHand, PokerHand secondHand) {
    this.firstHand = firstHand;
    this.secondHand = secondHand;
    this.firstHandRankValue = firstHand.getRankValue();
    this.secondHandRankValue = secondHand.getRankValue();
  }

  public String compare() {
    if (firstHand.getRank() != secondHand.getRank()) {
      return firstHand.getRank().ordinal() > secondHand.getRank().ordinal() ? PLAYER_1 : PLAYER_2;
    }
    else {
      return checkHighest();
    }
  }

  public String compareWhy() {
    String winner=compare();
    return PLAYER_1.equalsIgnoreCase(winner) ? winner+" " +firstHand.rankValue: winner+" "+secondHand.rankValue;
  }

  private String checkHighest() {
    if (firstHand.getRank() == Rank.HighCard) {
      return compareHighCard();
    }
    else if (firstHand.getRank() == Rank.OnePair) {
      return compareOnePair();
    }
    else if (firstHand.getRank() == Rank.TwoPairs) {
      return compareTwoPairs();
    }
    else if (firstHand.getRank() == Rank.ThreeOfAKind) {
      return compareThreeOfAKind();
    }
    else if (firstHand.getRank() == Rank.Straight) {
      return compareStraight();
    }
    else if (firstHand.getRank() == Rank.Flush) {
      return compareFlush();
    }
    else if (firstHand.getRank() == Rank.FullHouse) {
      return compareFullHouse();
    }
    else if (firstHand.getRank() == Rank.FourOfAKind) {
      return compareFourOfAKind();
    }
    else if (firstHand.getRank() == Rank.StraightFlush) {
      return compareStraightFlush();
    }
    else if (firstHand.getRank() == Rank.RoyalFlush) {
      return compareRoyalFlush();
    }
    else {
      return TIE;
    }
  }

  private String compareRoyalFlush() {
    return compareStraightFlush();
  }

  private String compareStraightFlush() {
    if (firstHandRankValue != secondHandRankValue) {
      return firstHandRankValue.ordinal() > secondHandRankValue.ordinal() ? PLAYER_1 : PLAYER_2;
    }
    else {
      return TIE;
    }
  }

  private String compareFourOfAKind() {
    return compareThreeOfAKind();
  }

  private String compareFullHouse() {
    return compareThreeOfAKind();
  }

  private String compareFlush() {
    return compareHighCard();
  }

  private String compareStraight() {
    return compareHighCard();
  }

  private String compareThreeOfAKind() {
    return firstHandRankValue.ordinal() > secondHandRankValue.ordinal() ? PLAYER_1 : PLAYER_2;
  }

  private String compareTwoPairs() {
    Value firstHandFirstPair = firstHand.getTwoPairsFirstPairValue();
    Value secondHandFirstPair = secondHand.getTwoPairsFirstPairValue();

    if (firstHandRankValue != secondHandRankValue) {
      return firstHandRankValue.ordinal() > secondHandRankValue.ordinal() ? PLAYER_1 : PLAYER_2;
    }
    else if (firstHandFirstPair != secondHandFirstPair) {
      return firstHandFirstPair.ordinal() > secondHandFirstPair.ordinal() ? PLAYER_1 : PLAYER_2;
    }
    else {
      return compareHighCard();
    }
  }

  private String compareOnePair() {

    if (firstHandRankValue != secondHandRankValue) {
      return firstHandRankValue.ordinal() > secondHandRankValue.ordinal() ? PLAYER_1 : PLAYER_2;
    }
    else {
      return compareHighCard();
    }
  }

  private String compareHighCard() {
    for (int i = firstHand.getCards().size() - 1; i >= 0; i--) {
      if (firstHand.getValue(i) != secondHand.getValue(i)) {
        return firstHand.getValue(i).ordinal() > secondHand.getValue(i).ordinal() ? PLAYER_1 : PLAYER_2;
      }
    }
    return TIE;
  }
}
