package poker.hand;

public enum Rank {
  HighCard,       //Highest value card.
  OnePair,        //Two cards of the same value.
  TwoPairs,       //Two different pairs.
  ThreeOfAKind,   //Three cards of the same value.
  Straight,       //All cards are consecutive values.
  Flush,          //All cards of the same suit.
  FullHouse,      //Three of a kind and a pair.
  FourOfAKind,    //Four cards of the same value.
  StraightFlush,  //All cards are consecutive values of same suit.
  RoyalFlush      // Ten, Jack, Queen, King, Ace, in same suit.
}
