package poker.hand;

import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.List;

public class PokerHandPairReader {

  public static List<PokerHand[]> readPairs(String fileName) {
    List<PokerHand[]> pokerHandPairs = new ArrayList<>();

    PokerHandFileReader.read(fileName).forEach(readPokerHandPairString -> {
      if (readPokerHandPairString.length() != 29) {
        throw new InputMismatchException();
      }
      PokerHand[] pokerHands = new PokerHand[2];
      pokerHands[0] = HandCreator.createPokerHand(readPokerHandPairString.substring(0, 14));
      pokerHands[1] = HandCreator.createPokerHand(readPokerHandPairString.substring(15));
      pokerHandPairs.add(pokerHands);
    });
    return pokerHandPairs;
  }
}
