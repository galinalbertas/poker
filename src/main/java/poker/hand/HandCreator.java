package poker.hand;

import poker.card.Card;
import poker.card.CardCreator;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.InputMismatchException;
import java.util.List;

public class HandCreator {
  private static final String SPLIT_STRING_TO_CHAR_ARRAY_REGEX = "(?!^)";
  private static final int POKER_HAND_LENGTH = 14;

  public static Hand createHand(String handString) {
    checkLength(handString);
    List<String> handStringList = Arrays.asList(handString.split(" "));
    List<Card> cards = new ArrayList<>();
    handStringList.forEach(cardString -> {
      String[] split = cardString.split(SPLIT_STRING_TO_CHAR_ARRAY_REGEX);
      cards.add(CardCreator.create(split[0], split[1]));
    });
    return new Hand(cards);
  }

  public static PokerHand createPokerHand(String handString) {
    checkLength(handString);
    List<String> handStringList = Arrays.asList(handString.split(" "));
    List<Card> cards = new ArrayList<>();
    handStringList.forEach(cardString -> {
      String[] split = cardString.split(SPLIT_STRING_TO_CHAR_ARRAY_REGEX);
      cards.add(CardCreator.create(split[0], split[1]));
    });
    return new PokerHand(cards);
  }

  private static void checkLength(String handString) {
    if (handString.length() != POKER_HAND_LENGTH) {
      throw new InputMismatchException();
    }
  }
}
