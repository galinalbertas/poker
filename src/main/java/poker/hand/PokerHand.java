package poker.hand;

import poker.card.Card;
import poker.card.Value;

import java.util.HashSet;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.stream.Collectors;

public class PokerHand extends Hand {

  Rank rank;
  Value rankValue;

  public PokerHand(List<Card> cards) {
    super(cards);
    setRank();
  }

  public Rank getRank() {
    return rank;
  }

  public Value getRankValue() {
    return rankValue;
  }

  private boolean isRoyalFlush() {
    sortByValue();
    return getLowestValue() == Value.Ten
           && getHighestValue() == Value.Ace
           && isOneSuite();
  }

  private boolean isStraightFlush() {
    return !isRoyalFlush() && isCardsConsecutiveValue() && isOneSuite();
  }

  private boolean isFourOfAKind() {
    return new HashSet<>(getCardValueList().subList(0, 4)).size() == 1
           || new HashSet<>(getCardValueList().subList(1, 5)).size() == 1;
  }

  private boolean isFullHouse() {
    sortByValue();
    List<Value> firstThree = getCardValueList().subList(0, 3);
    List<Value> lastTwo = getCardValueList().subList(3, 5);
    List<Value> firstTwo = getCardValueList().subList(0, 2);
    List<Value> lastThree = getCardValueList().subList(2, 5);

    return new HashSet<>(firstThree).size() == 1 && new HashSet<>(lastTwo).size() == 1
           || new HashSet<>(firstTwo).size() == 1 && new HashSet<>(lastThree).size() == 1;
  }

  private Value getFullHouseValue() {
    sortByValue();
    List<Value> firstThree = getCardValueList().subList(0, 3);
    List<Value> lastThree = getCardValueList().subList(2, 5);

    return new HashSet<>(firstThree).size() == 1 ?
           firstThree.get(0) : lastThree.get(0);
  }

  private boolean isFlush() {
    return isOneSuite() && !isRoyalFlush() && !isStraightFlush();
  }

  private boolean isThreeOfAKind() {
    if (isFullHouse() || isFourOfAKind()) {
      return false;
    }
    sortByValue();
    return new HashSet<>(getCardValueList().subList(0, 3)).size() == 1
           || new HashSet<>(getCardValueList().subList(1, 4)).size() == 1
           || new HashSet<>(getCardValueList().subList(2, 5)).size() == 1;
  }

  private Value getThreeOfAKindValue() {
    return findDuplicateValue().first();
  }

  private boolean isTwoPairs() {
    if (isFullHouse() || isFourOfAKind() || isThreeOfAKind()) {
      return false;
    }
    sortByValue();
    return
      (new HashSet<>(getCardValueList().subList(0, 2)).size() == 1 && new HashSet<>(getCardValueList().subList(2, 4)).size() == 1)
      || (new HashSet<>(getCardValueList().subList(0, 2)).size() == 1 && new HashSet<>(getCardValueList().subList(3, 5)).size() == 1)
      || (new HashSet<>(getCardValueList().subList(1, 3)).size() == 1 && new HashSet<>(getCardValueList().subList(3, 5)).size() == 1);
  }

  private Value getTwoPairsValue() {
    return findDuplicateValue().last();
  }

  protected Value getTwoPairsFirstPairValue() {
    return findDuplicateValue().first();
  }

  private boolean isStraight() {
    return isCardsConsecutiveValue() && !isStraightFlush();
  }

  private boolean isOnePair() {
    return new HashSet<>(getCardValueList()).size() == 4;
  }

  private Value getOnePairValue() {
    return findDuplicateValue().first();
  }

  private boolean isHighCard() {
    return new HashSet<>(getCardValueList()).size() == 5
           && !isOneSuite() && !isStraight();
  }

  private SortedSet<Value> findDuplicateValue() {
    Set<Value> values = new HashSet<>();
    return getHandSortedByValue().getCardValueList().stream()
                                 .filter(n -> !values.add(n)).collect(Collectors.toCollection(TreeSet::new));
  }

  private void setRank() {
    if (isRoyalFlush()) {
      rank = Rank.RoyalFlush;
      rankValue = Value.Ace;
    }
    else if (isStraightFlush()) {
      rank = Rank.StraightFlush;
      rankValue = getHighestValue();
    }
    else if (isFourOfAKind()) {
      rank = Rank.FourOfAKind;
      rankValue = findDuplicateValue().first();
    }
    else if (isFullHouse()) {
      rank = Rank.FullHouse;
      rankValue = getFullHouseValue();
    }
    else if (isFlush()) {
      rank = Rank.Flush;
      rankValue = getHighestValue();
    }
    else if (isStraight()) {
      rank = Rank.Straight;
      rankValue = getHighestValue();
    }
    else if (isThreeOfAKind()) {
      rank = Rank.ThreeOfAKind;
      rankValue = getThreeOfAKindValue();
    }
    else if (isTwoPairs()) {
      rank = Rank.TwoPairs;
      rankValue = getTwoPairsValue();
    }
    else if (isOnePair()) {
      rank = Rank.OnePair;
      rankValue = getOnePairValue();
    }
    else if (isHighCard()) {
      rank = Rank.HighCard;
      rankValue = getHighestValue();
    }
    else {
      throw new InputMismatchException();
    }
  }
}
