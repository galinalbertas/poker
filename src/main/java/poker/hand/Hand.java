package poker.hand;

import poker.card.Card;
import poker.card.Value;
import poker.card.Suite;

import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class Hand {
  private static final int ONE = 1;
  private List<Card> cards;

  public Hand(List<Card> cards) {
    this.cards = cards;
  }

  protected List<Card> getCards() {
    return cards;
  }

  protected void sortByValue() {
    cards = getCardsSortedByValue();
  }

  protected Hand getHandSortedByValue() {
    sortByValue();
    return this;
  }

  protected List<Card> getCardsSortedByValue() {
    return cards.stream().sorted(Comparator.comparing(Card::getValue)).collect(Collectors.toList());
  }

  protected List<Value> getCardValueList() {
    return cards
      .stream()
      .map(Card::getValue)
      .collect(Collectors.toList());
  }

  protected List<Suite> getCardSuiteList() {
    return cards
      .stream()
      .map(Card::getSuite)
      .collect(Collectors.toList());
  }

  protected Set<Suite> getSuiteSet() {
    return new HashSet<>(getCardSuiteList());
  }

  protected Value getHighestValue() {
    return Collections.max(getCardValueList());
  }

  protected Value getLowestValue() {
    return Collections.min(getCardValueList());
  }

  protected boolean isOneSuite() {
    return getSuiteSet().size() == ONE;
  }

  protected Value getSecondHighestValue() {
    return getCardsSortedByValue().get(3).getValue();
  }

  protected Value getThirdHighestValue() {
    return getCardsSortedByValue().get(2).getValue();
  }

  protected Value getValue(int i) {
    if (i < 0 || i > cards.size()) {
      throw new InputMismatchException();
    }
    return getCardsSortedByValue().get(i).getValue();
  }

  protected boolean isCardsConsecutiveValue() {
    List<Card> sortedByValue = getCardsSortedByValue();
    for (int i = 1; i < sortedByValue.size(); i++) {
      if ((sortedByValue.get(i).getValue().ordinal() - sortedByValue.get(i - 1).getValue().ordinal()) != 1) {
        return false;
      }
    }
    return true;
  }

  @Override
  public String toString() {
    return cards.toString();
  }
}
