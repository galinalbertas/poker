package poker;

import poker.hand.PokerHandPairComparator;
import poker.hand.PokerHandPairReader;

public class app {
  public static void main(String[] args) {
    System.out.println("Player 1 wins " + generateHowManyHandsDoesPlayer1Win() + " hands.");
  }

  public static long generateHowManyHandsDoesPlayer1Win() {
    return PokerHandPairReader
      .readPairs("p054_poker.txt")
      .stream()
      .map(hp -> new PokerHandPairComparator(hp[0], hp[1]).compare())
      .filter(PokerHandPairComparator.PLAYER_1::equals)
      .count();
  }
}
