package poker.card;

public enum Value {
  Two("2"), Three("3"), Four("4"), Five("5"), Six("6"), Seven("7"), Eight("8"), Nine("9"), Ten("T"),
  Jack("J"), Queen("Q"), King("K"), Ace("A");

  private String val;

  Value(String val) {
    this.val = val;
  }

  public String value() {
    return val;
  }
}
