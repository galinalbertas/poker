package poker.card;

import java.util.Objects;

public class Card {

  private final Value value;
  private final Suite suite;

  public Card(Value value, Suite suite) {
    this.value = value;
    this.suite = suite;
  }

  public Value getValue() {
    return value;
  }

  public Suite getSuite() {
    return suite;
  }

  @Override
  public String toString() {
    return
      value.value() +
      suite.value();
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Card card = (Card)o;
    return value == card.value &&
           suite == card.suite;
  }

  @Override
  public int hashCode() {
    return Objects.hash(value, suite);
  }
}
