package poker.card;

import java.util.InputMismatchException;

public class CardCreator {
  public CardCreator() {
  }

  public static Card create(String value, String suite) {
    return new Card(setValue(value), setSuite(suite));
  }

  private static Value setValue(String rank) {
    switch (rank) {
      case "2":
        return Value.Two;
      case "3":
        return Value.Three;
      case "4":
        return Value.Four;
      case "5":
        return Value.Five;
      case "6":
        return Value.Six;
      case "7":
        return Value.Seven;
      case "8":
        return Value.Eight;
      case "9":
        return Value.Nine;
      case "T":
        return Value.Ten;
      case "J":
        return Value.Jack;
      case "Q":
        return Value.Queen;
      case "K":
        return Value.King;
      case "A":
        return Value.Ace;
    }
    throw new InputMismatchException();
  }

  private static Suite setSuite(String suite) {
    switch (suite) {
      case "H":
        return Suite.Hearts;
      case "C":
        return Suite.Clubs;
      case "D":
        return Suite.Diamonds;
      case "S":
        return Suite.Spades;
    }
    throw new InputMismatchException();
  }
}
