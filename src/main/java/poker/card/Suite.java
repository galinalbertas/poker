package poker.card;

public enum Suite {
  Clubs("C"), Diamonds("D"), Hearts("H"), Spades("S");

  private String val;

  Suite(String val) {
    this.val = val;
  }

  public String value() {
    return val;
  }
}
